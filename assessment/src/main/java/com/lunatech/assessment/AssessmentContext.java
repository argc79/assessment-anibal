package com.lunatech.assessment;

import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lunatech.assessment.model.Airport;
import com.lunatech.assessment.model.Country;
import com.lunatech.assessment.model.Runway;

/**
 * This context class loads and holds a structure to be used by the query and report service
 * 
 * @author anibal
 *
 */
public final class AssessmentContext {
	private static final Logger LOGGER = LoggerFactory.getLogger(AssessmentContext.class);
	private final NavigableMap<String, Country> countryNames;
	private final Map<String, Country> countryCodes;
	private final Map<String, Airport> airports;
	private final List<Runway> runways;
	
	private AssessmentContext() {
		countryNames = new TreeMap<>();
		countryCodes = new HashMap<>();
		airports = new HashMap<>();
		runways = new ArrayList<>();
		init();
	}
	
	private static class LazyHolder {
        private static final AssessmentContext INSTANCE = new AssessmentContext();
    }
	
	public static AssessmentContext getInstance() {
		return LazyHolder.INSTANCE;
	}
	
	/**
	 * The CSV files are loaded and set into a different structures
	 */
	private void init() {
		try {
			final Reader countryReader = new FileReader("src/main/resources/countries.csv");
			final Reader runwayReader = new FileReader("src/main/resources/runways.csv");
			final Reader airportReader = new FileReader("src/main/resources/airports.csv");
			final Iterable<CSVRecord> countryRecords = CSVFormat.EXCEL.withHeader().parse(countryReader);
			final Iterable<CSVRecord> runwayRecords = CSVFormat.EXCEL.withHeader().parse(runwayReader);
			final Iterable<CSVRecord> airportRecords = CSVFormat.EXCEL.withHeader().parse(airportReader);
			
			initCountries(countryRecords);
			initAirports(airportRecords);
			initRunways(runwayRecords);
			
		} catch (Exception e) {
			LOGGER.error("The context could not be created", e);
		}
	}
	
	private void initCountries(final Iterable<CSVRecord> countryRecords) {
		for (CSVRecord country : countryRecords) {
		    final String name = country.get("name");
		    final String code = country.get("code");
		    final Country c = new Country(country);
		    countryCodes.put(code.toUpperCase(), c);
		    countryNames.put(name.toLowerCase(), c);
		}
	}
	
	private void initAirports(final Iterable<CSVRecord> airportRecords) {
		for (final CSVRecord airport: airportRecords) {
			final String isoCountry = airport.get("iso_country");
			if (countryCodes.containsKey(isoCountry)) {
				final Country country = countryCodes.get(isoCountry);
				final Airport a = new Airport(country, airport);
				country.addAirport(a);
				airports.put(airport.get("id"), a);
			}
		}
	}
	
	private void initRunways(final Iterable<CSVRecord> runwayRecords) {
		for (final CSVRecord runway: runwayRecords) {
			final String airportRef = runway.get("airport_ref");
			if (airports.containsKey(airportRef)) {
				final Airport airport = airports.get(airportRef);
				final Runway r = new Runway(airport, runway);
				airport.addRunway(r);
				runways.add(r);
			}
		}
	}
	
	public NavigableMap<String, Country> getCountryNames() {
		return countryNames;
	}

	public Map<String, Country> getCountryCodes() {
		return countryCodes;
	}
	
	public Collection<Country> getCountries() {
		return countryCodes.values();
	}

	public Map<String, Airport> getAirports() {
		return airports;
	}

	public List<Runway> getRunways() {
		return runways;
	}
}
