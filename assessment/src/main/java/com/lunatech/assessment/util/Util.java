package com.lunatech.assessment.util;

/**
 * This class contains helpful methods
 * 
 * @author anibal
 *
 */
public class Util {
	public static boolean isEmptyString(final String s) {
		return s == null || s.length() == 0;
	}
}
