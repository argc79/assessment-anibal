package com.lunatech.assessment.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVRecord;

/**
 * It contains the Airport CSV record
 *  
 * @author anibal
 */
public class Airport {
	private enum Columns {
		ID("id"), NAME("name"), ISO_COUNTRY("iso_country");

		private final String name;

		private Columns(final String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}
	}
	
	private final CSVRecord airportRecord;
	private final Country country;
	private final List<Runway> runways;
	
	public Airport(final Country country, final CSVRecord airport) {
		this.country = country;
		this.airportRecord = airport;
		runways = new ArrayList<>();
	}
	
	public Country getCountry() {
		return country;
	}
		
	
	public CSVRecord getAirport() {
		return airportRecord;
	}
	
	public List<Runway> getRunways() {
		return runways;
	}
	
	public void addRunway(final Runway runway) {
		runways.add(runway);
	}
	
	public String getIsoCountry() {
		return airportRecord.get(Columns.ISO_COUNTRY.getName());
	}
}