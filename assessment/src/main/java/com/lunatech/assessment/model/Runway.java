package com.lunatech.assessment.model;

import org.apache.commons.csv.CSVRecord;

/**
 * It contains the Runway CSV record
 *  
 * @author anibal
 */
public class Runway {
	private enum Columns {
		ID("id"), SURFACE("surface"), LATITUDE("le_ident");

		private final String name;

		private Columns(final String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}
	}
	
	private final CSVRecord runwayRecord;
	private final Airport airport;
	
	public Runway(final Airport airport, final CSVRecord runway) {
		this.airport = airport;
		this.runwayRecord = runway;
	}
	
	public CSVRecord getRunway() {
		return runwayRecord;
	}
	
	public Airport getAirport() {
		return airport;
	}
	
	public String getSurface() {
		return runwayRecord.get(Columns.SURFACE.getName());
	}
	public String getLatitude() {
		return runwayRecord.get(Columns.LATITUDE.getName());
	}
	
}