package com.lunatech.assessment.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVRecord;

/**
 * It contains the Country CSV record
 *  
 * @author anibal
 */
public class Country {

	private enum Columns {
		ID("id"), CODE("code"), NAME("name"), CONTINENT("continent"), WIKIPEDIA_LINK(
				"wikipedia_link"), KEYWORDS("keywords");

		private final String name;

		private Columns(final String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}
	}

	private final CSVRecord countryRecord;
	private final List<Airport> airports;

	public Country(final CSVRecord countryRecord) {
		this.countryRecord = countryRecord;
		airports = new ArrayList<>();
	}

	public CSVRecord getCountry() {
		return countryRecord;
	}

	public List<Airport> getAirports() {
		return airports;
	}

	public void addAirport(final Airport airport) {
		airports.add(airport);
	}

	public String getId() {
		return countryRecord.get(Columns.ID.getName());
	}
	
	public String getCode() {
		return countryRecord.get(Columns.CODE.getName());
	}
	
	public String getName() {
		return countryRecord.get(Columns.NAME.getName());
	}
	
}