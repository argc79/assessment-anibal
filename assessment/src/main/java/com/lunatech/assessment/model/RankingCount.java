package com.lunatech.assessment.model;

import java.util.Comparator;

/**
 * This class hold a counter and two comparators to be used by different collections
 * 
 * @author anibal
 */
public class RankingCount {

	public static final Comparator<RankingCount> ASC = new Comparator<RankingCount>() {
		public int compare(final RankingCount cc1, final RankingCount cc2) {
			return cc1.getCount().compareTo(cc2.getCount());
		}
	};
	public static final Comparator<RankingCount> DESC = new Comparator<RankingCount>() {
		public int compare(final RankingCount cc1, final RankingCount cc2) {
			return cc2.getCount().compareTo(cc1.getCount());
		}
	};
	
	private final String name;
	private final Integer count;

	public RankingCount(final String name, final Integer count) {
		this.name = name;
		this.count = count;
	}

	public String getName() {
		return name;
	}

	public Integer getCount() {
		return count;
	}

}