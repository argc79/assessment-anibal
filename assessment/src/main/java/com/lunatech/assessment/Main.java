package com.lunatech.assessment;

import java.util.List;
import java.util.Scanner;

import com.lunatech.assessment.model.Country;
import com.lunatech.assessment.model.RankingCount;
import com.lunatech.assessment.service.QueryService;
import com.lunatech.assessment.service.QueryServiceImpl;
import com.lunatech.assessment.service.ReportService;
import com.lunatech.assessment.service.ReportServiceImpl;

/**
 * Client for Query & Report service
 *  
 * @author anibal
 */
public class Main {
	private static final int TOP_NUMBER = 10;
	private static final String OPTION_ONE_MENU = "1";
	private static final String OPTION_TWO_MENU = "2";
	private static final String OPTION_THREE_MENU = "3";
	private static final String OPTION_FOUR_MENU = "4";
	private static final String OPTION_FIVE_MENU = "5";
	
	public static void main(final String[] args) {
		final Main main = new Main();
		final Scanner sc = new Scanner(System.in);
		main.mainMenu(sc);
	}

	public void mainMenu(final Scanner sc) {
		System.out
				.println("Please, select one of the following options: \r1- Query\r2- Report\r3- Exit");
		final String line = sc.next();
		if (OPTION_ONE_MENU.equals(line)) {
			queryCountryOptionMenu(sc);
		} else if (OPTION_TWO_MENU.equals(line)) {
			reportMenu(sc);
		} else if (OPTION_THREE_MENU.equals(line)) {
			sc.close();
			System.exit(0);
			return;
		} else {
			System.out.println("Invalid option");
			mainMenu(sc);
		}
	}

	private void reportMenu(final Scanner sc) {
		System.out.println("Please, select one of the following options: "
				+ "\r1- Top countries by most number of airports"
				+ "\r2- Top countries by least number of airports"
				+ "\r3- Top most common latitudes"
				+ "\r4- Runway type per country" + "\r5- Back");
		final String line = sc.next();
		if (OPTION_ONE_MENU.equals(line)) {
			reportTopCountriesByMostNumberOfAirportsMenu(sc);
		} else if (OPTION_TWO_MENU.equals(line)) {
			reportTopCountriesByLeastNumberOfAirportsMenu(sc);
		} else if (OPTION_THREE_MENU.equals(line)) {
			reportTopMostCommonLatitudes(sc);
		} else if (OPTION_FOUR_MENU.equals(line)) {
			reportRunwayTypePerCountry(sc);
		} else if (OPTION_FIVE_MENU.equals(line)) {
			mainMenu(sc);
			return;
		} else {
			System.out.println("Invalid option, back to main menu");
			mainMenu(sc);
		}
	}

	private void reportRunwayTypePerCountry(final Scanner sc) {
		final ReportService rs = ReportServiceImpl.getInstance();
		System.out.println("\nType of runways per country");
		for (String countryName : rs.getRunwayTypePerCountry().keySet()) {
			System.out.println(countryName);
			for (String runwayType : rs.getRunwayTypePerCountry().get(
					countryName)) {
				System.out.printf("\t%s\n", runwayType);
			}
		}
		reportMenu(sc);
	}

	private void reportTopMostCommonLatitudes(final Scanner sc) {
		final ReportService rs = ReportServiceImpl.getInstance();
		System.out.println("\nTop 10 Most Common Latitudes");
		for (RankingCount cc : rs
				.getTopMostCommonLatitudes(TOP_NUMBER)) {
			System.out.printf("%s - %d\n", cc.getName(), cc.getCount());
		}
		reportMenu(sc);
	}

	private void reportTopCountriesByLeastNumberOfAirportsMenu(final Scanner sc) {
		final ReportService rs = ReportServiceImpl.getInstance();
		System.out.println("\nTop 10 countries with least amount of airports");
		for (RankingCount cc : rs
				.getTopCountriesByLeastNumberOfAirports(TOP_NUMBER)) {
			System.out.printf("%s - %d\n", cc.getName(), cc.getCount());
		}
		reportMenu(sc);
	}

	private void reportTopCountriesByMostNumberOfAirportsMenu(final Scanner sc) {
		final ReportService rs = ReportServiceImpl.getInstance();
		System.out.println("Top 10 countries with most amount of airports");
		for (RankingCount cc : rs
				.getTopCountriesByMostNumberOfAirports(TOP_NUMBER)) {
			System.out.println(String.format("%s - %d", cc.getName(),
					cc.getCount()));
		}
		reportMenu(sc);
	}

	public void queryCountryOptionMenu(final Scanner sc) {
		System.out.println("Please, insert type of query: \r1- By name\r2- By country code");
		final String line = sc.next();
		if (OPTION_ONE_MENU.equals(line)) {
			queryCountryByCountryNameMenu(sc);
		} else if (OPTION_TWO_MENU.equals(line)) {
			queryCountryByCountryCodeMenu(sc);
		} else if (OPTION_THREE_MENU.equals(line)) {
			sc.close();
			System.exit(0);
			return;
		} else {
			System.out.println("Invalid option");
			mainMenu(sc);
		}
		
		mainMenu(sc);
	}

	private void queryCountryByCountryCodeMenu(final Scanner sc) {
		final QueryService qs = QueryServiceImpl.getInstance();
		System.out.println("Please, insert Country code below:");
		final String line = sc.next();
		final Country country = qs.getCountryByCode(line);
		showCountry(country);
		mainMenu(sc);
	}
	
	private void queryCountryByCountryNameMenu(final Scanner sc) {
		final QueryService qs = QueryServiceImpl.getInstance();
		
		System.out.println("Please, insert query below:");
		final String line = sc.next();
		final List<Country> countries = qs.getCountriesByNameFuzzy(line);
		if (countries.size() == 0) {
			System.out.println("No countries");
		} else {
			countries.stream().forEach(c -> showCountry(c));
		}
		mainMenu(sc);
	}

	public void showCountry(final Country country) {
		if (country == null) {
			System.out.println("No country");
			return;
		}
		System.out.println(country.getCountry().get("name"));
		country.getAirports().forEach(
				airport -> {
					System.out.println(String.format("\t%s [%s]", airport
							.getAirport().get("name"), airport.getAirport()
							.get("iata_code")));
					airport.getRunways().forEach(
							runway -> {
								System.out.println("\t\t"
										+ runway.getRunway().get("id"));
							});
				});
	}
}
