package com.lunatech.assessment.service;

import java.util.ArrayList;
import java.util.List;
import java.util.NavigableMap;

import org.apache.commons.lang3.StringUtils;

import static com.lunatech.assessment.util.Util.isEmptyString;

import com.lunatech.assessment.AssessmentContext;
import com.lunatech.assessment.model.Country;

/**
 * This is the implementation of the QueryService interface
 * 
 * @author anibal
 *
 */
public final class QueryServiceImpl implements QueryService {

	private static final int LEVENSHTEIN_DISTANCE = 3;
	private AssessmentContext context;

	private QueryServiceImpl() {
		context = AssessmentContext.getInstance();
	}

	private static class LazyHolder {
		private static final QueryServiceImpl INSTANCE = new QueryServiceImpl();
	}

	public static QueryServiceImpl getInstance() {
		return LazyHolder.INSTANCE;
	}

	/**
	 * @see QueryService#getCountriesByNameFuzzy(String)
	 */
	public List<Country> getCountriesByNameFuzzy(final String countryNamePattern) {
		if (isEmptyString(countryNamePattern)) {
			return null;
		}
		final List<Country> results = new ArrayList<>();
		final String countryNamePatternLowerCase = countryNamePattern
				.toLowerCase();
		for (final String countryName : context.getCountryNames().keySet()) {
			if (countryName.contains(countryNamePatternLowerCase)
					|| StringUtils.getLevenshteinDistance(countryName,
							countryNamePatternLowerCase) < LEVENSHTEIN_DISTANCE) {
				results.add(context.getCountryNames().get(countryName));
			}
		}
		return results;
	}

	/**
	 * @see QueryService#getCountriesByNameLeftMatching(String)
	 */
	public List<Country> getCountriesByNameLeftMatching(
			final String countryNamePattern) {
		if (isEmptyString(countryNamePattern)) {
			return null;
		}
		final List<Country> results = new ArrayList<>();
		final NavigableMap<String, Country> countryNames = context
				.getCountryNames();
		final String countryKey = countryNames.ceilingKey(countryNamePattern
				.toLowerCase());
		if (countryKey == null) {
			return results;
		}
		Country country = countryNames.get(countryKey);
		while (country != null
				&& country.getName().toLowerCase()
						.startsWith(countryNamePattern.toLowerCase())) {
			results.add(country);
			final String higherKey = countryNames.higherKey(country.getName()
					.toLowerCase());
			country = isEmptyString(higherKey) ? null : countryNames
					.get(higherKey);
		}
		return results;
	}

	/**
	 * @see QueryService#getCountryByCode(String)
	 */
	public Country getCountryByCode(final String countryCode) {
		if (isEmptyString(countryCode)) {
			return null;
		}
		return context.getCountryCodes().get(countryCode.toUpperCase());
	}

}
