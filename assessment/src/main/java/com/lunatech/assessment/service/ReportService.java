package com.lunatech.assessment.service;

import java.util.Map;
import java.util.Set;

import com.lunatech.assessment.model.RankingCount;

/**
 * This interface contains the report method(s) for the assessment
 * 
 * @author anibal
 *
 */
public interface ReportService {

	/**
	 * Gets a sorted RankingCount array with countries with the most number of
	 * airports
	 * 
	 * @param rankingSize size of the ranking
	 * @return an array
	 */
	RankingCount[] getTopCountriesByMostNumberOfAirports(final int rankingSize);

	/**
	 * Gets a sorted RankingCount array with countries with the least number of
	 * airports
	 * 
	 * @param rankingSize size of the ranking
	 * @return an array
	 */
	RankingCount[] getTopCountriesByLeastNumberOfAirports(final int rankingSize);

	/**
	 * Gets a sorted RankingCount array with countries with the most common latitudes
	 * 
	 * @param rankingSize size of the ranking
	 * @return an array
	 */
	RankingCount[] getTopMostCommonLatitudes(final int rankingSize);

	/**
	 * Gets a map with the runway type per country
	 * 
	 * @return a Map
	 */
	Map<String, Set<String>> getRunwayTypePerCountry();
}
