package com.lunatech.assessment.service;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import com.lunatech.assessment.AssessmentContext;
import com.lunatech.assessment.model.Country;
import com.lunatech.assessment.model.RankingCount;
import com.lunatech.assessment.model.Runway;

import static com.lunatech.assessment.util.Util.isEmptyString;

/**
 * This is the implementation of the ReportService interface
 * 
 * @author anibal
 *
 */
public final class ReportServiceImpl implements ReportService {

	private final Map<String, Integer> airportCountsByCountryCode;
	private final Map<String, Integer> latitudeCounts;
	private final Map<String, Set<String>> runwayTypesPerCountry;
	private final AssessmentContext context;

	private static class LazyHolder {
		private static final ReportServiceImpl INSTANCE = new ReportServiceImpl();
	}

	private ReportServiceImpl() {
		airportCountsByCountryCode = new HashMap<>();
		latitudeCounts = new HashMap<>();
		runwayTypesPerCountry = new TreeMap<>();
		context = AssessmentContext.getInstance();
		init();
	}

	public static ReportServiceImpl getInstance() {
		return LazyHolder.INSTANCE;
	}

	private void init() {
		for (final Country country : context.getCountries()) {
			airportCountsByCountryCode.put(country.getName(), country
					.getAirports().size());
		}
		for (final Runway runway : context.getRunways()) {
			if (!isEmptyString(runway.getSurface())) {
				final String countryName = runway.getAirport().getCountry()
						.getName();
				if (!getRunwayTypePerCountry().containsKey(countryName)) {
					getRunwayTypePerCountry().put(countryName,
							new TreeSet<String>());
				}
				getRunwayTypePerCountry().get(countryName).add(
						runway.getSurface());
			}
			latitudeCounts.put(runway.getLatitude(),
					latitudeCounts.getOrDefault(runway.getLatitude(), 0) + 1);
		}
	}

	/**
	 * @see ReportService#getTopCountriesByMostNumberOfAirports(int)
	 */
	public RankingCount[] getTopCountriesByMostNumberOfAirports(
			final int rankingSize) {
		return getTopRankingCount(airportCountsByCountryCode, rankingSize,
				RankingCount.ASC);
	}

	/**
	 * @see ReportService#getTopCountriesByLeastNumberOfAirports(int)
	 */
	public RankingCount[] getTopCountriesByLeastNumberOfAirports(
			final int rankingSize) {
		return getTopRankingCount(airportCountsByCountryCode, rankingSize,
				RankingCount.DESC);
	}

	/**
	 * @see ReportService#getTopMostCommonLatitudes(int)
	 */
	public RankingCount[] getTopMostCommonLatitudes(final int rankingSize) {
		return getTopRankingCount(latitudeCounts, rankingSize, RankingCount.ASC);
	}

	/**
	 * @see ReportService#getRunwayTypePerCountry()
	 */
	public Map<String, Set<String>> getRunwayTypePerCountry() {
		return runwayTypesPerCountry;
	}

	private RankingCount[] getTopRankingCount(
			final Map<String, Integer> countsMap, final int rankingSize,
			final Comparator<RankingCount> comparator) {
		final RankingCount[] results = new RankingCount[rankingSize];
		final PriorityQueue<RankingCount> heap = new PriorityQueue<RankingCount>(
				rankingSize, comparator);
		for (final String key : countsMap.keySet()) {
			final Integer count = countsMap.get(key);
			final RankingCount countryCount = new RankingCount(key, count);
			if (heap.size() < rankingSize) {
				heap.offer(countryCount);
			} else if (comparator.compare(countryCount, heap.peek()) > 0) {
				heap.poll();
				heap.offer(countryCount);
			}
		}
		for (int i = rankingSize - 1; i >= 0; i--) {
			results[i] = heap.poll();
		}
		return results;
	}

}
