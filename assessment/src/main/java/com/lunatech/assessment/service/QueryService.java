package com.lunatech.assessment.service;

import java.util.List;

import com.lunatech.assessment.model.Country;

/**
 * This interface contains the query method(s) for the assessment
 * 
 * @author anibal
 */
public interface QueryService {

	/**
	 * It performs a fuzzy search based on the Levenshtein distance. i.e the word Calada has
	 * a distance of 1, which is the number of differences to achieve the word Canada
	 * 
	 * performs O(n) 
	 * 
	 * @param countryNamePattern
	 *            the query
	 * @return the list of Country cotaining airports and runways
	 */
	List<Country> getCountriesByNameFuzzy(final String countryNamePattern);

	/**
	 * It performs a LeftMatching search.
	 * performs at best O(1)
	 * 
	 * @param countryNamePattern
	 * @return
	 */
	List<Country> getCountriesByNameLeftMatching(final String countryNamePattern);
	
	/**
	 * It performs a country code search
	 * performs O(1)
	 * 
	 * @param countryCode 
	 * @return
	 */
	Country getCountryByCode(final String countryCode);
}
