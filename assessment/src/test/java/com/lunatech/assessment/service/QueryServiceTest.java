package com.lunatech.assessment.service;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.lunatech.assessment.model.Country;

public class QueryServiceTest extends BaseTest {

	@Test
	public void queryByCountryName() {
		final QueryService qs = getQueryService();
		List<Country> countriesByNameFuzzy = qs.getCountriesByNameFuzzy("basil");
		assertEquals(countriesByNameFuzzy.size(), 1);
		assertEquals("Brazil", countriesByNameFuzzy.get(0).getName());
		
		countriesByNameFuzzy = qs.getCountriesByNameFuzzy("belum");
		assertEquals(countriesByNameFuzzy.size(), 1);
		assertEquals("Belgium", countriesByNameFuzzy.get(0).getName());
		
		countriesByNameFuzzy = qs.getCountriesByNameFuzzy("dfasgsdgsaghagsafgdsagdsa");
		assertEquals(0, countriesByNameFuzzy.size());
		
		countriesByNameFuzzy = qs.getCountriesByNameFuzzy("south");
		assertEquals(5, countriesByNameFuzzy.size());
		
		List<Country> countriesByNameLeftMatching = qs.getCountriesByNameLeftMatching("zim");
		assertEquals(1, countriesByNameLeftMatching.size());
		assertEquals("Zimbabwe", countriesByNameLeftMatching.get(0).getName());
		
		countriesByNameLeftMatching = qs.getCountriesByNameLeftMatching("south");
		assertEquals(4, countriesByNameLeftMatching.size());
		
		countriesByNameLeftMatching = qs.getCountriesByNameLeftMatching("feqrewqgtweqtewq");
		assertEquals(0, countriesByNameLeftMatching.size());
	}

	@Test
	public void queryByCountryCode() {
		final QueryService qs = getQueryService();
		Country countryByCode = qs.getCountryByCode("US");
		assertNotNull(countryByCode);
		
		countryByCode = qs.getCountryByCode("PPP");
		assertNull(countryByCode);
	}
}
