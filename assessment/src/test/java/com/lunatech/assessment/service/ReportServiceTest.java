package com.lunatech.assessment.service;

import static org.junit.Assert.*;

import java.util.Map;
import java.util.Set;

import org.junit.Test;

import com.lunatech.assessment.model.RankingCount;

public class ReportServiceTest extends BaseTest {

	@Test
	public void topCountriesByMostNumberOfAirports() {
		final ReportService reportService = getReportService();
		final RankingCount[] result = reportService
				.getTopCountriesByMostNumberOfAirports(10);

		assertNotNull(result);
		assertEquals(10, result.length);
		assertEquals("United States", result[0].getName());
	}

	@Test
	public void topCountriesByLeastNumberOfAirports() {
		final ReportService reportService = getReportService();
		final RankingCount[] result = reportService
				.getTopCountriesByLeastNumberOfAirports(10);

		assertNotNull(result);
		assertEquals(10, result.length);
		assertEquals("South Georgia and the South Sandwich Islands",
				result[0].getName());
	}

	@Test
	public void topMostCommonLatitudes() {
		final ReportService reportService = getReportService();
		final RankingCount[] result = reportService
				.getTopMostCommonLatitudes(10);

		assertNotNull(result);
		assertEquals(10, result.length);
		assertEquals("H1", result[0].getName());
	}

	@Test
	public void runwayTypePerCountry() {
		final ReportService reportService = getReportService();
		final Map<String, Set<String>> runwayTypePerCountry = reportService
				.getRunwayTypePerCountry();
		assertArrayEquals(new String[] { "ASP", "BIT", "GRE", "GRS", "MAC" },
				runwayTypePerCountry.get("Zimbabwe").toArray(new String[0]));
		assertArrayEquals(new String[] { "ASP", "CON", "Concrete", "GRE", "MET", "STONE", "UNK" },
				runwayTypePerCountry.get("Argentina").toArray(new String[0]));
	}
}
