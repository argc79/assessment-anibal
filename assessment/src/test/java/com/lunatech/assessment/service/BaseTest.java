package com.lunatech.assessment.service;


/**
 * Basic test functionality
 * @author anibal
 *
 */
public abstract class BaseTest {
	
	public QueryService getQueryService() {
		return QueryServiceImpl.getInstance();
	}
	
	public ReportService getReportService() {
		return ReportServiceImpl.getInstance();
	}
}
